<?php
/**
 Template Name: Contest
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1570827322960926');
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1"
src="https://www.facebook.com/tr?id=1570827322960926&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<style type="text/css">
	/*********************
	MAILCHIMP <:|
	*********************/
	#mc_embed_signup2 {
		background-color: #302c2c !important;
	}

	#mc_embed_signup2 form {
		padding: 0px !important;
		background-color: #302c2c !important;
	}

	#mc_embed_signup2 .mc-field-group {
		padding: 0px !important;
		min-height: inherit !important;
		width: 100% !important;
	}
	#mc_embed_signup2 input {
		border: 2px solid #ed9803 !important;
		border-radius: 0px !important;

	}
	#mc_embed_signup2 input[type="text"],input[type="email"], select, textarea, .field {
		background-color: #302c2c !important;
		margin: 0px 0px 5px 0px !important;
		font-family: "Open Sans", sans-serif !important;
		color: white !important;
	}
	input[placeholder="email"] {
		color: white !important;
		margin: 0px 0px 5px 0px !important;
	}
#mc_embed_signup2{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	#mc_embed_signup2 .button {
		width: 100% !important;
		background-color: #ed9803 !important;
		border: none !important;
		padding: 10px !important;
		font-weight: bold !important;
		color: #1b1a1c !important;
		letter-spacing: 1px !important;
		font-family: "Open Sans", sans-serif !important;
		&:hover {
			background-color: #1b1a1c !important;
			color: #ed9803 !important;
			@include transition(.5s);
		}
	}

	div#mc_embed_signup_scroll2 {
		width: 100% !important;
	}

	#mc-embedded-subscribe2 {margin: 0px;}

	/*********************
	BOTTOM MAILCHIMP <:|
	*********************/

		div#mc_embed_signup3_scroll {
			width: 270px !important;
			display:block !important;
			margin: auto !important;
		}

		#mc_embed_signup3 {
			background-color: #ed9803 !important;
			width: 100% !important;
			max-width: 430px !important;
			margin: auto !important;
			color: white !important;
		}
		#mc_embed_signup3 .button, #mc_embed_signup3 input.email {
			font-weight: 400 !important;
			border-radius: 0px !important;
			border: 2px solid white !important;
			background-color: transparent !important;
			height: 38px !important;
			line-height: 31px;
			font-size: 14px !important;
			color: white !important;

		}
		#mc_embed_signup3 .button {
			float: right !important;
			width: 70px !important;
			margin: -43px 2px 0px 0px !important;
			padding: 0px !important;
		}
		#mc_embed_signup3 .button:hover {
			background-color: white !important;
			color: #302c2c !important;
			@include transition(.3s);
		}
		#mc_embed_signup3 input.email {
			float: left !important;
			width: 200px !important;
			font-weight: 400 !important;
			padding: 0px 18px !important;
		}

		#mc_embed_signup3 input.email::-webkit-input-placeholder { color: white !important; }
	  #mc_embed_signup3 input.email::-moz-placeholder { color: white !important; }
	  #mc_embed_signup3 input.email:-ms-input-placeholder { color: white !important; }
	  #mc_embed_signup3 input.email:-moz-placeholder { color: white !important; }

		.response, .mce_inline_error {
			color: white;
			margin: 20px;
			font-family: "Open Sans", sans-serif;
		}
		.response a {
			color: #ed9803;
				text-decoration: none;
		}

</style>

<!-- hijacking: on/off - animation: none/scaleDown/rotate/gallery/catch/opacity/fixed/parallax -->

	<section class="topSec">

    <div class="lower">
      <div id="t1" class="person">
        <div  data-aos="fade-down"><img class="pIcon" src="/wp-content/themes/k8y_101/library/images/peakIco.png"/></div>
        <div data-aos="fade-up"><img class="pPerson" src="/wp-content/themes/k8y_101/library/images/peakF.png"/></div>
      </div>
      <div id="t2" class="person">
        <div data-aos="fade-down"><img class="pIcon" src="/wp-content/themes/k8y_101/library/images/brainIco.png"/></div>
        <div data-aos="fade-up"><img class="pPerson" src="/wp-content/themes/k8y_101/library/images/brainF.png"/></div>
      </div>
      <div id="t3" class="title" data-aos="fade-up">
        <img class="logo" src="/wp-content/themes/k8y_101/library/images/h2o_logoWhite@2x.png"/>
        <div class="info">
        <h3>Win a</h3>
        <h1>Drip Party</h1>
        <h3>For you &amp; 3 friends</h3>
      </div>
      </div>
      <div id="t4" class="person">
        <div data-aos="fade-down"><img class="pIcon" src="/wp-content/themes/k8y_101/library/images/radIco.png"/></div>
        <div data-aos="fade-up"><img class="pPerson" src="/wp-content/themes/k8y_101/library/images/radF.png"/></div>
      </div>
      <div id="t5" class="person">
        <div data-aos="fade-down"><img class="pIcon" src="/wp-content/themes/k8y_101/library/images/socialIco.png"/></div>
        <div data-aos="fade-up"><img class="pPerson" src="/wp-content/themes/k8y_101/library/images/socialF.png"/></div>
      </div>

  </div>
	</section>

  <section class="middleSec">
    <div class="middleWrap">
      <div class="instructions" data-aos="fade-up">
        <div class="enter">
          <h3>To enter</h3>
          <h4 style="margin-bottom: 10px;">1. Like us on Facebook </h4>
					<div><?php echo fb_like_button(); ?></div>
          <h4>2. Enter Your Info Below</h4>
        </div>
      <!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
 We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup2">
<form action="https://gogeddit.us2.list-manage.com/subscribe/post?u=4c27f09ab6a622c6340c21d60&amp;id=580de731b3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div id="mc_embed_signup_scroll2">

<div class="mc-field-group">
<input type="email" value="" name="EMAIL" class="required email" placeholder="email"  id="mce-EMAIL">
</div>
<div class="mc-field-group">
<input type="text" value="" name="FNAME" class="" placeholder="first name"  id="mce-FNAME">
</div>
<div class="mc-field-group">
<input type="text" value="" name="LNAME" class="" placeholder="last name"  id="mce-LNAME">
</div>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none">Oops! Try again.</div>
<div class="response" id="mce-success-response" style="display:none">Submission Recieved. Good luck!</div>
</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4c27f09ab6a622c6340c21d60_580de731b3" tabindex="-1" value=""></div>
<div class="clear"><input type="submit" value="ENTER" name="subscribe" id="mc-embedded-subscribe2" class="button"></div>
</div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
<h6>Deadline: Nov 30</h6>
</div>
</div>
  </section>

<section class="bottomSec">
  <div class="bottomWrap">
  <h3 data-aos="fade-right">Win a <span id="oCall">drip party</span> for you &amp; three friends.</h3>
  <h5 data-aos="fade-right" class="blackOut" style="width: fit-content;">6PM | Friday, December 8</h5>

  <div class="imageF">
    <div class="imageD" data-aos="fade-up">
      <div class="backimg" style="background:url('/wp-content/themes/k8y_101/library/images/tv.jpg');">

      <h5 data-aos="fade-right" class="blackOut" id="b1">SCREENING OF YOUR</h5>
      <h5 data-aos="fade-right" class="blackOut" id="b2">CHOICE ON OUR 75" TV</h5>

    </div>
    </div>
    <div class="imageD" data-aos="fade-up">
      <div class="backimg" style="background:url('/wp-content/themes/k8y_101/library/images/ivBag.jpg');">
      <h5 data-aos="fade-right" class="blackOut" id="b1">1 FREE FULL SIZED DRIP</h5>
      <h5 data-aos="fade-right" class="blackOut" id="b2">COCKTAIL PER GUEST</h5>
    </div>
    <p>*excluding High Dose Vit. C, IV NAD Therapy and Body Detox<br/><a href="http://healthhydrationoasis.com/cocktails/" target="_blank" >See full cocktail menu here</a></p>
    </div>
    <div class="imageD" data-aos="fade-up">
      <div class="backimg" style="background:url('/wp-content/themes/k8y_101/library/images/aps.jpg');">
      <h5 data-aos="fade-right" class="blackOut" id="b1">APPETIZERS COURTESY</h5>
      <h5 data-aos="fade-right" class="blackOut" id="b2">OF ABV SOCIAL</h5>
    </div>
    </div>
  </div>
</div>
</section>

<div class="emailSignUp" style="background-color: #ed9803;">
	<h3>Sign up to receive updates!</h3>
	<!-- Begin MailChimp Signup Form -->
	<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
	<div id="mc_embed_signup3">
	<form action="//gogeddit.us2.list-manage.com/subscribe/post?u=4c27f09ab6a622c6340c21d60&amp;id=14ecea09d3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	    <div id="mc_embed_signup_scroll">
		<input style="color: white !important;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email" required>
	    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4c27f09ab6a622c6340c21d60_14ecea09d3" tabindex="-1" value=""></div>
	    <div class="clear"><input style="margin: -56px 0px 0px 197px !important !important;" type="submit" value="send" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
	    </div>
	</form>
	</div>

	<!--End mc_embed_signup-->
</div>


<?php get_footer(); ?>
