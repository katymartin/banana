<?php get_header(); ?>
<section class="blogPost"  data-aos="fade-up">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="blogGrad"></div>
	<div class="blogBanner" data-aos="fade-up">
		<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div  style="background: url('<?php echo $thumb['0'];?>'); height: 60vh; background-repeat: no-repeat; background-size: cover; background-position: center center; "  ></div>
	</div>

	<div class="blogContent">
		<div class="titleInfo">
		<h1><?php the_title(); ?></h1>
		<h5><?php the_date(); ?></h5>
	</div>
		<p><?php the_content(); ?></p>
	</div>

<?php endwhile; ?>
<?php endif; ?>
</section>

<?php get_footer(); ?>
