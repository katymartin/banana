/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


jQuery(document).ready(function($) {

	AOS.init({
					easing: 'ease-in-out-sine'
				});

				AOS.init({
			       disable: 'mobile'
			     });
				  /*******
				    MENU
				  *******/

				  $(document).ready(function () {
				    //Menu button on click event
				    $('.mobile-nav-button').on('click', function() {
				      // Toggles a class on the menu button to transform the burger menu into a cross
				      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
				      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");
				      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");

				      // Toggles a class that slides the menu into view on the screen
				      $('.mobileMenu').toggleClass('mobileMenu--open');
				      return false;
				    });
				  });



/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function
/*
TweenMax.from(".greySlog, .whiteSlog, .orangeSlog", 2, {x: -300});

*/

//Slick Slider for Projects


var $slider = $('.home-slide').slick({
	infinite: true,
 speed: 500,
 autoplay: true,
 dots: true,
 arrows: true,
 cssEase: 'linear',
 lazyLoad: 'ondemand',
 prevArrow: '<a class="slick-prev fa">&#xf104;</a>',
 nextArrow: '<a class="slick-next fa">&#xf105;</a>',

		 });


/**** Featured Cocktail 1  ****/



	var main=function(event){
	$('.ftIngS1').click(function(event){
	    $('.ingX1').toggleClass('ftOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ftIngS1').click(function(event){
			$('.faS1').toggleClass('ftFa');
	});};
$(document).ready(main);


/**** Featured Cocktail 2  ****/


	var main=function(event){
	$('.ftIngS2').click(function(event){
	    $('.ingX2').toggleClass('ftOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ftIngS2').click(function(event){
			$('.faS2').toggleClass('ftFa');
	});};
$(document).ready(main);


/**** Featured Cocktail 3  ****/


	var main=function(event){
	$('.ftIngS3').click(function(event){
	    $('.ingX3').toggleClass('ftOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ftIngS3').click(function(event){
			$('.faS3').toggleClass('ftFa');
	});};
$(document).ready(main);


/**** Featured Cocktail 4  ****/


	var main=function(event){
	$('.ftIngS4').hover(function(event){
	    $('.ingX4').toggleClass('ftOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ftIngS4').hover(function(event){
			$('.faS4').toggleClass('ftFa');
	});};
$(document).ready(main);

/**** Cocktail 5  ****/


	var main=function(event){
	$('.ingS5').click(function(event){
	    $('.ingX5').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS5').click(function(event){
			$('.faS5').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 6 ****/


	var main=function(event){
	$('.ingS6').click(function(event){
	    $('.ingX6').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS6').click(function(event){
			$('.faS6').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 7  ****/


	var main=function(event){
	$('.ingS7').click(function(event){
	    $('.ingX7').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS7').click(function(event){
			$('.faS7').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 8  ****/


	var main=function(event){
	$('.ingS8').click(function(event){
	    $('.ingX8').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS8').click(function(event){
			$('.faS8').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 9  ****/


	var main=function(event){
	$('.ingS9').click(function(event){
	    $('.ingX9').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS9').click(function(event){
			$('.faS9').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 10  ****/


	var main=function(event){
	$('.ingS10').click(function(event){
	    $('.ingX10').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS10').click(function(event){
			$('.faS10').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 11  ****/


	var main=function(event){
	$('.ingS11').click(function(event){
	    $('.ingX11').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS11').click(function(event){
			$('.faS11').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 12  ****/

	var main=function(event){
	$('.ingS12').click(function(event){
	    $('.ingX12').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS12').click(function(event){
			$('.faS12').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 13  ****/


	var main=function(event){
	$('.ingS13').click(function(event){
	    $('.ingX13').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS13').click(function(event){
			$('.faS13').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 14  ****/


	var main=function(event){
	$('.ingS14').click(function(event){
	    $('.ingX14').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS14').click(function(event){
			$('.faS14').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 15  ****/


	var main=function(event){
	$('.ingS15').click(function(event){
	    $('.ingX15').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS16').click(function(event){
			$('.faS16').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 16  ****/


	var main=function(event){
	$('.ingS16').click(function(event){
	    $('.ingX16').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS16').click(function(event){
			$('.faS16').toggleClass('faOpen');
	});};
$(document).ready(main);


/**** Cocktail 17  ****/

	var main=function(event){
	$('.ingS17').click(function(event){
	    $('.ingX17').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS17').click(function(event){
			$('.faS17').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 18  ****/

	var main=function(event){
	$('.ingS18').click(function(event){
	    $('.ingX18').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS18').click(function(event){
			$('.faS18').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 19  ****/


	var main=function(event){
	$('.ingS19').click(function(event){
	    $('.ingX19').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS19').click(function(event){
			$('.faS19').toggleClass('faOpen');
	});};
$(document).ready(main);

/**** Cocktail 20  ****/


	var main=function(event){
	$('.ingS20').click(function(event){
	    $('.ingX20').toggleClass('ingOpen');
	});};
	$(document).ready(main);

	var main=function(event){
	$('.ingS20').click(function(event){
			$('.faS20').toggleClass('faOpen');
	});};






/*
 * Put all your regular jQuery in here.
*/

/*
 * Let's fire off the gravatar function
 * You can remove this if you don't need it
*/
loadGravatars();


}); /* end of as page load scripts */
