<?php
/**
 Template Name: Membership Checkout
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


<head>
	<title>Contact Information</title>
	<meta name="description" content="Employee contact and location information.">
</head>

<body>
	<div id="wrapper">
	<section class="headerSec" data-aos="fade-left">
		<h2>Checkout</h2>
	</section>
<section class="checkWrap">
	<?php echo do_shortcode('[download_checkout]'); ?>
</section>

</div>
<div style="display: none;">
<?php get_footer(); ?>
</div>
