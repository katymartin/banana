<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



<head>
		<title><?php wp_title(''); ?></title>
	 	<?php wp_head(); ?>

	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="content-language" content="nl">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

<link href="/wp-content/themes/k8y_101/library/css/slick.css" rel="stylesheet">
<link href="/wp-content/themes/k8y_101/library/css/slick-theme.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

<link rel="canonical" href="http://healthhydrationoasis.com/" />
				<meta property="og:url" content="http://healthhydrationoasis.com/"/>
				<meta property="og:title" content="H2O | Health Hydration Oasis" />
				<meta property="og:description" content="Milwaukee based IV hydration service restoring you, one drop at a time." />
				<meta property="og:type" content="article" />
				<meta property="og:image" content="http://healthhydrationoasis.com/wp-content/themes/k8y_101/library/images/h2o_FBPreview.png" />
				<meta property="og:image:width" content="1200" />
				<meta property="og:image:height" content="628" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39015905-31"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-39015905-31');
</script>


</head>

<nav>
<a class="home" href="/">
	<svg class="h20logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 101" data-aos="fade-right">
   <path class="orange_logo" d="M292.7 2c1.4.7 2.7 1.7 3.8 3 1.1 1.3 1.9 2.9 2.6 4.9.7 1.9 1 4.1 1 6.4v39.6c0 2.5-.3 4.7-1 6.6-.7 1.9-1.5 3.5-2.6 4.8-1.1 1.3-2.3 2.3-3.8 3-1.4.7-2.9 1-4.3 1H209c-1.5 0-3-.3-4.4-1-1.4-.7-2.6-1.7-3.7-3s-1.9-2.9-2.6-4.8c-.7-1.9-1-4.1-1-6.6V16.3c0-2.4.3-4.6 1-6.5.7-1.9 1.5-3.6 2.6-4.9 1.1-1.3 2.3-2.3 3.7-3 1.4-.7 2.8-1 4.4-1h79.4c1.4 0 2.8.4 4.3 1.1zm-6.4 52c.9-1.2 1.3-2.8 1.3-4.7V22.8c0-1.9-.4-3.5-1.1-4.7-.7-1.2-1.8-1.8-3.2-1.8h-69.5c-1.5 0-2.6.6-3.3 1.8-.7 1.2-1 2.8-1 4.7v26.5c0 1.9.4 3.5 1.2 4.7.8 1.2 1.7 1.8 2.9 1.8h69.6c1.2 0 2.2-.6 3.1-1.8z"/>
   <path class="orange_line_logo" d="M87 .6v28.2H15.6V.6H0v70h15.6V44.4H87v26.2h15.6V.6z"/>
   <path class="drop_logo" d="M163.7 85.5h-30v-8c0-1.4.3-2.5 1-3.3.7-.8 1.5-1.2 2.4-1.2h21.7c.3 0 .6-.1.9-.4s.4-.7.4-1.2V71c0-.6-.1-1-.3-1.2-.2-.2-.5-.3-.9-.3h-25.1V65h26.6c.9 0 1.7.4 2.4 1.2.7.8 1 1.9 1 3.3V73c0 1.4-.3 2.5-1 3.3-.7.8-1.5 1.2-2.4 1.2h-21.8c-.4 0-.7.1-.9.3-.2.2-.3.6-.3 1.2v2h26.4v4.5z"/>
   <path class="drop_shine_logo" d="M147 19.5s-18.9 14-9.7 25.2c7.2 8.7 14.8 5.3 18.4 1.4 9.1-9.5-1.2-20-8.7-26.6zm.2 3.2c1.7.8 8.2 6.5 10 14.4 0 0 .1.2 0 0-.6-1.5-2.6-5.8-10-14.4z"/>
   <path class="orange_logo" d="M167.6 8.9h-41.1c-1.2 0-2.3.5-3.2 1.3-.8.8-1.3 1.9-1.3 3.2v78.3c0 1.2.5 2.3 1.3 3.2.8.8 1.9 1.3 3.2 1.3h41.1c1.2 0 2.3-.5 3.2-1.3.8-.8 1.3-1.9 1.3-3.2V13.3c0-1.2-.5-2.3-1.3-3.2-.8-.7-1.9-1.2-3.2-1.2m-41.1-4h41.1c2.3 0 4.4 1 6 2.5 1.5 1.5 2.5 3.6 2.5 6v78.3c0 2.3-1 4.4-2.5 6-1.5 1.5-3.6 2.5-6 2.5h-41.1c-2.3 0-4.4-1-6-2.5-1.5-1.5-2.5-3.6-2.5-6V13.3c0-2.3 1-4.4 2.5-6 1.6-1.5 3.7-2.4 6-2.4z"/>
 </svg>
 <i class="fa fa-home" aria-hidden="true"></i>
</a>
<a class="phone" href="tel:12622771018" target="_blank"><i class="fa fa-phone-square" aria-hidden="true"></i></a>
<div class="deskNav">

<ul>

		<li><a href="/cocktails/" target="_self">COCKTAILS</a></li>
		<li><a href="/pricing/" target="_self">PRICING</a></li>
		<li><a href="/media/" target="_self">MEDIA</a></li>
		<li><a href="/about/" target="_self">about</a></li>
		<li><a href="/faq/" target="_self">FAQ</a></li>
		<li><a href="/contact/" target="_self">CONTACT</a></li>


			 <li><a href="tel:12622771018" target="_blank"><i class="fa fa-phone-square" aria-hidden="true"></i></a></li>
			<li><a href="https://www.facebook.com/H2Omke/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
			<li><a href="https://www.instagram.com/h2o_mke/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li id="bookNow"><a href="/book/" target="_self">BOOK NOW</a></li>
	</ul>
</div>

<div class="mobile-nav-button">
	<div class="mobile-nav-button__line"></div>
	<div class="mobile-nav-button__line"></div>
	<div class="mobile-nav-button__line"></div>
</div>

<div class="mobileMenu">

	<ul>
			<li><a href="/cocktails/" target="_self">COCKTAILS</a></li>
			<li><a href="/pricing/" target="_self">PRICING</a></li>
			<li><a href="/media/" target="_self">MEDIA</a></li>
			<li><a href="/about/" target="_self">ABOUT</a></li>
			<li><a href="/faq/" target="_self">FAQ</a></li>
			<li><a href="/contact/" target="_self">CONTACT</a></li>
			<li id="bookNowM"><a href="/book/" target="_self">BOOK NOW</a></li>
			<ul id="mobile">
			<li><a href="tel:12622771018" target="_blank"><i class="fa fa-phone-square" aria-hidden="true"></i></a></li>
			<li ><a href="https://www.facebook.com/H2Omke/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
			<li><a href="https://www.instagram.com/h2o_mke/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
		</ul>
	</ul>
</div>
</nav>
