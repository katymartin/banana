<?php
/**
 Template Name: Contact
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


<head>
	<title>Contact Information</title>
	<meta name="description" content="Employee contact and location information.">
</head>

<body>
	<div id="wrapper">
	<section class="headerSec" data-aos="fade-left">
		<h2>Contact</h2>
	</section>
<section class="contactPage">


	<div class="flex" data-aos="fade-up">
		<div class=" contactForm2">
			<h1>Questions?<br/>Feel free to call<br/>or message.</h1>
				<a href="tel:12622771018" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;262.277.1018</p></a>
				<a href="mailto:h2o@hydrationoasis.com"><p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;h2o@hydrationoasis.com</p></a>
				<a href="https://www.google.com/maps/place/17110+W+Greenfield+Ave+%236,+Brookfield,+WI+53005/@43.0169422,-88.1282744,17z/data=!3m1!4b1!4m5!3m4!1s0x88050633b079296d:0x676bde1817613055!8m2!3d43.0169422!4d-88.1260804" target="_blank"><p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;17110&nbsp;W.&nbsp;Greenfield&nbsp;Ave,&nbsp;#6</p>
				<p style="padding-left: 32px; margin-top: 0px;">Brookfield, Wisconsin, 53005</p></a>

		</div>
		<div class=" contactForm2">
			<?php echo do_shortcode("[ninja_form id=1]"); ?>

		</div>
	</div>

<div class="emailSignUp">
	<h3>Sign up to receive updates!</h3>
	<!-- Begin MailChimp Signup Form -->
	<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
		/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
		   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	</style>
	<div id="mc_embed_signup">
	<form action="//gogeddit.us2.list-manage.com/subscribe/post?u=4c27f09ab6a622c6340c21d60&amp;id=14ecea09d3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	    <div id="mc_embed_signup_scroll">
		<input style="color: white !important;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email" required>
	    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4c27f09ab6a622c6340c21d60_14ecea09d3" tabindex="-1" value=""></div>
	    <div class="clear"><input type="submit" value="send" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
	    </div>
	</form>
	</div>

	<!--End mc_embed_signup-->
</div>

<div ><?php include 'map.php';?> </div>


</section>
</div>
</body>
<div style="display: none;">
<?php get_footer(); ?>
</div>
