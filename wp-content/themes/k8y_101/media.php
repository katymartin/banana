<?php
/**
 Template Name: Media
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>H2O Blog</title>
	<meta name="description" content="We offer a large variety of hydration cocktails, all with their own unique healing properties.">
</head>

<body>
	<div id="wrapper">
<div class="headerSec" data-aos="fade-left">
	<h2>BLOG</h2>
</div>

	<section class="vlog"  data-aos="fade-up">
		<div class="blogContF" >
		<?php
			$args = array(
					'post_type' => 'vlog',
					'post_status' => 'publish',
					'posts_per_page' => 100,
					'order' => 'DESC'
			);
			$projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
						$projects_loop->the_post();

				 							$youtube = get_field('youtube_link');
											$perma = get_permalink();
											$title = get_the_title();
				 							$date =  get_the_date();

				 	          ?>

							<div class="blogContD">
								<div class="embed-container">
									<iframe src="<?php echo $youtube; ?>" frameborder="0" allowfullscreen></iframe>
								</div>
								<div class="postData">
								<a href="<?php echo $perma; ?>" target="_self"><h3><?php echo $title; ?></h3></a>
								<h5><?php echo $date; ?></h5>
								</div>
							</div>


							<?php
								endwhile;
							wp_reset_postdata();
							endif;
							?>
</div>
	</section>


	<section class="blog"  data-aos="fade-up">
		<div class="blogContF" >
		<?php
			$args = array(
					'post_type' => 'blog',
					'post_status' => 'publish',
					'posts_per_page' => 100,
					'order' => 'DESC'
			);
			$projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
						$projects_loop->the_post();


											$perma = get_permalink();
											$title = get_the_title();
				 							$date =  get_the_date();

										?>

							<div class="blogContD">

								<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	              <a href="<?php echo $perma; ?>" target="_self"><div  style="background: url('<?php echo $thumb['0'];?>'); height: 350px; background-repeat: no-repeat; background-size: cover; background-position: center center; "  ></div></a>

								<div class="postData">
								<a href="<?php echo $perma; ?>" target="_self"><h3><?php echo $title; ?></h3></a>
								<h5><?php echo $date; ?></h5>
								</div>
							</div>


							<?php
								endwhile;
							wp_reset_postdata();
							endif;
							?>
</div>
	</section>



		<div class="headerSec" data-aos="fade-left">
			<h2>IN THE MEDIA</h2>
		</div>






			<section class="vlog"  data-aos="fade-up">
				<div class="blogContF">
				<?php
					$args = array(
							'post_type' => 'mediavideo',
							'post_status' => 'publish',
							'posts_per_page' => 100,
							'order' => 'DESC'
					);
					$projects_loop = new WP_Query($args);
					if ($projects_loop->have_posts()):
							while ($projects_loop->have_posts()):
								$projects_loop->the_post();

								$youtube = get_field('youtube_link');
								$extlink = get_field('external_link');
								$title = get_the_title();
								$date =  get_the_date();

							?>

				<div class="blogContD">
					<div class="embed-container">
						<iframe src="<?php echo $youtube; ?>" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="postData">
					<a href="<?php echo $extlink; ?>" target="_self"><h3><?php echo $title; ?></h3></a>
					<h5><?php echo $date; ?></h5>
					</div>
				</div>
									<?php
										endwhile;
									wp_reset_postdata();
									endif;
									?>
</div>
			</section>


				<section class="blog"  style="margin-bottom: 80px;" data-aos="fade-up">
					<div class="blogContF">
					<?php
						$args = array(
								'post_type' => 'mediaarticle',
								'post_status' => 'publish',
								'posts_per_page' => 100,
								'order' => 'DESC'
						);
						$projects_loop = new WP_Query($args);
						if ($projects_loop->have_posts()):
								while ($projects_loop->have_posts()):
									$projects_loop->the_post();


									$extlink = get_field('external_link');
									$title = get_the_title();
									$date =  get_the_date();

								?>

					<div class="blogContD">
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<a href="<?php echo $extlink; ?>" target="_self"><div  style="background: url('<?php echo $thumb['0'];?>'); height: 350px; background-repeat: no-repeat; background-size: cover; background-position: center center; "  ></div></a>
						<div class="postData">
						<a href="<?php echo $extlink; ?>" target="_self"><h3><?php echo $title; ?></h3></a>
						<h5><?php echo $date; ?></h5>
						</div>
					</div>


					<?php
						endwhile;
					wp_reset_postdata();
					endif;
					?>
										</div>
				</section>




</div>
</body>



<?php get_footer(); ?>
