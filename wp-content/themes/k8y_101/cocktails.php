<?php
/**
 Template Name: Cocktails
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Cocktail Menu</title>
	<meta name="description" content="We offer a large variety of hydration cocktails, all with their own unique healing properties.">
</head>

<body>
	<div id="wrapper">
<section class="headerSec" data-aos="fade-left">
	<h2>Cocktail Menu</h2>
</section>

	<section class="featuredCocktail"  data-aos="fade-left">
				<?php
	          $recent = new WP_Query( array(
	              'post_type' => 'cocktail',   /* edit this line */
	              'posts_per_page' => 2,
	              'order' => 'DESC') );
	              ?>

	            <?php
 							$counter = 1;
							while ( $recent->have_posts() ) : $recent->the_post();
	              ?>
								<?php

				 							$bp0 = get_field('cocktail_icon');
				 	            $bp1 = get_field('cocktail_price');
				 	            $bp2 = get_field('cocktail_sub');
				 	            $bp3 = get_field('cocktail_name');
				 							$bp4 = get_field('cocktail_use');
				 							$bp5 = get_field('cocktail_list');
				 							$bp6 = get_field('cocktail_ing_slide');
				 							$bp7 = get_field('cocktail_ing');
				 							$bpcolor = get_field('color_back');
				 							$bpcolor2 = get_field('color_text');
				 	          ?>

								<div class="cocktail" >
								<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	              <div class="backImg ingX<?php echo $counter ?>" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; "  >
									<img class="icon" src="<?php echo $bp0;?>">


									<div class="cocktailInfo">

										<h2 style="margin-top: -30px;" id="<?php echo $bpcolor2;?>"><?php echo $bp3;?></h2>
										<h5><?php echo $bp4;?></h5>
										<p class="useList"><?php echo $bp5;?></p>
									</div>

									<div class="ingSlide" id="<?php echo $bpcolor;?>">

									</div>

								</div>

								<div class="ingSec" id="<?php echo $bpcolor;?>">
									<p class="ing"><?php echo $bp7;?></p>
								</div>

							</div>
							<?php $counter++; ?>
	            <?php endwhile; ?>

	</section>






		<section class="cocktailSec">
			<?php
					$cocktail = new WP_Query( array(
							'post_type' => 'cocktail',   /* edit this line */
							'posts_per_page' => 100,
							'offset' => 2,
							'order' => 'DESC') );
			?>

			<?php
			$counter = 5;
			while ( $cocktail->have_posts() ) : $cocktail->the_post(); ?>

			 <?php
			 				$bp00 = get_field('cocktail_img');
							$bp0 = get_field('cocktail_icon');
	            $bp2 = get_field('cocktail_sub');
	            $bp3 = get_field('cocktail_name');
							$bp4 = get_field('cocktail_use');
							$bp5 = get_field('cocktail_list');
							$bp6 = get_field('cocktail_ing_slide');
							$bp7 = get_field('cocktail_ing');
							$bpcolor = get_field('color_back');
							$bpcolor2 = get_field('color_text');
	          ?>
						<div class="cocktail ingS<?php echo $counter ?>" data-aos="fade-up">
							<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
								<div class="cocktailIcon" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;">
								<img class="icon" src="<?php echo $bp0;?>">
							</div>




								<div class="ingSec ingX<?php echo $counter ?>" id="<?php echo $bpcolor;?>">
									<div class="cocktailInfo">
										<h5><?php echo $bp4;?></h5>
										<p class="useList"><?php echo $bp5;?></p>
									</div>
								</div>
								<div class="ingSlide" id="<?php echo $bpcolor;?>">
									<h2 ><?php echo $bp3;?></h2>
									<!--
									<div class="ingDiv"><h4><i class="fa fa-caret-up faS< ?php echo $counter ?>" aria-hidden="true">
									</i>< ?php echo $bp6;?><i class="fa fa-caret-up faS< ?php echo $counter ?>" aria-hidden="true"></i></h4>
										</div>
									-->
								</div>

						</div>
						<?php $counter++; ?>
		<?php endwhile; ?>

	</section>

	<div class="disclaimer">
		<p>*These statements have not been evaluated by the Food and Drug Administration. These products are not intended to diagnose, treat, cure, or prevent any disease.</p>
	</div>
</div>
</body>



<?php get_footer(); ?>
