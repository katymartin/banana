<?php
/**
 Template Name: About
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>About | H2O</title>
	<meta name="description" content="Learn about our staff and how we became one of the first hyrdration therapy locations in Milwaukee.">

	<link rel='stylesheet prefetch' href="/wp-content/themes/k8y_101/library/css/boostrap.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css'>
</head>

<body>
	<div id="wrapper">
<section class="headerSec" data-aos="fade-left">
	<h2>About</h2>
</section>

<section class="bio">
		<div class="bioFlex">
				<div class="bioFlexPic" data-aos="fade-right">
					<img src="/wp-content/themes/k8y_101/library/images/Alia_HeadShot3.jpg" alt="Founder, Alia Fox Headshot"/>
				</div>
				<div class="bioFlexCopy" data-aos="fade-up">
					<h1>Alia L Fox, MD</h1>
					<h2>Founder and Medical Director</h2>
					<p>Dr. Fox began her career in medicine after receiving her Medical Doctorate in 2007, followed by a postgraduate degree in Anesthesiology.  She completed her residency in Anesthesiology from the Cleveland Clinic, a nationally and internationally top-ranked hospital for multiple specialties.  She continued on to complete her fellowship in Pediatric Anesthesiology at the Children’s Hospital of Wisconsin, and is board certified in both Adult and Pediatric Anesthesiology. </p>
					<p>With previous staff positions at Duke University followed by Madison, WI, Dr. Fox currently practices in the private sector for a large Milwaukee hospital system.  As the Department of Anesthesia Chair and Medical Director of one of these local hospitals, she continues to see the spectrum of adult and pediatric cases which vary from the simple to very complex. </p>
					<p>Stemming from her own experience of receiving IV’s over the years, Dr. Fox hopes to use IV nutrient therapy as a means to improve people’s lives in an easier and more comfortable setting:</p>
					<p></p>

				</div>
		</div>
		<div class="bioQuote" data-aos="fade-up">
			<h3>  “As a healthcare provider, I’ve seen firsthand how the hustle of today’s life impacts us. We try our best to make the right choices, but sometimes life doesn’t allow us to do that. Wellness is a complex prize to win, but I do understand that it starts from within. IV’s and hydration is what I do every single day in the OR, and I want all of these benefits to be available mainstream. So I invite you to embark on this journey with me, and together we can experience what it means to be restored one drop a time.”</h3>
		</div>
</section>

<section class="aboutGallery">
	<section class="headerSec" data-aos="fade-left">
		<h2>OUR SPACE</h2>
	</section>
	<div id="gallery" class="container-fluid">

		 <!--
		<video class="card vid" controls>
	    <source src="http://techslides.com/demos/sample-videos/small.mp4" type="video/mp4">
	    </source>
	  </video>
	-->

	<?php the_content();?></p>
		<?php
			if( have_rows('about_gallery') ):
			while( have_rows('about_gallery') ): the_row(); ?>

			<img src="<?php the_sub_field('gallery_img'); ?>" class="card img-responsive">

	<?php endwhile; endif; ?>




	</div>

	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      
	      </div>
	    </div>

	  </div>
	</div>
</section>

</div>
</body>


<?php get_footer(); ?>

<script>
/*******
	ABOUT IMAGE GALLERY
*******/

$(document).ready(function(){
	$("img").click(function(){
	var t = $(this).attr("src");
	$(".modal-body").html("<img src='"+t+"' class='modal-img'>");
	$("#myModal").modal();
});

$("video").click(function(){
	var v = $("video > source");
	var t = v.attr("src");
	$(".modal-body").html("<video class='model-vid' controls><source src='"+t+"' type='video/mp4'></source></video>");
	$("#myModal").modal();
});
});//EOF Document.ready
</script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
