<?php
/**
 Template Name: Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>H2O Milwaukee</title>
	<meta name="description" content="Milwaukee based IV hydration service restoring you, one drop at a time.">
</head>

<body>
<div id="wrapper">

<!--
	<div>< ?php include 'old_header.php';?></div>
-->

<!---
<section class="home-slide">

		< ?php if( have_rows('home_banner') ): ?>
		< ?php while ( have_rows('home_banner') ) : the_row();?>
			<div class="slideWrap">
			<img class="mobileImg" src="< ?php the_sub_field('mobile_image'); ?>"/>
			<img class="deskImg" src="< ?php the_sub_field('desk_image'); ?>"/>
	<div class="learnMore">
		<h3>< ?php the_sub_field('slide_title'); ?></h3>
		<div class="bannerButton" id="< ?php the_sub_field('button_color'); ?>">
		<a  href="< ?php the_sub_field('slide_button_link'); ?>" target="_self"><p>< ?php the_sub_field('slide_button_cta'); ?></p></a>
	</div>
	</div>
</div>
< ?php endwhile; endif; ?>
</section>

--->

<section class="youTube">
	<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/AeTxhsVjt8I?version=3&feature=player_embedded&autohide=1&autoplay=1&loop=1&rel=0&controls=0&showinfo=0&feature=player_embedded' frameborder='0' allowfullscreen></iframe></div>
</section>



<section class="pColWrap" >
	<div class="pCols" data-aos="fade-up">
	<div>
		<p >
			We are a new health and wellness boutique where we provide custom IV cocktail therapy to help heal and rejuvenate. This is done in a modern and upscale setting that meets the standard expectations of a traditional healthcare experience without the associated hassle.
		</p>
	</div>
	<div>
		<p>
			From treating the common cold, facilitating athletic recovery, or NAD therapy and beyond, we are a modern and innovative approach to rejuvenating the body from the inside out.
		</p>
		<p style="color: #ef5b2e; font-weight: bold;">Restoring you, one drop at a time.</p>
	</div>
</div>
</section>


<section class="homeAd"  style="background-color:<?php the_field('background'); ?>">
	<div><a href="<?php the_field('ad_link'); ?>"><img class="mobileAd" src="<?php the_field('mobile_ad'); ?>" alt="<?php the_field('mobile_alt'); ?>"/></a>
	<a href="<?php the_field('ad_link'); ?>"><img class="deskAd" src="<?php the_field('desk_ad'); ?>" alt="<?php the_field('desk_alt'); ?>"  data-aos="fade-up"/></a>
</section>

<section class="headerSec" data-aos="fade-left">
	<h2>Featured Cocktails</h2>
</section>

<section style="padding: 70px 4% 40px 4%;" class="cocktailSec" >

	<?php
			$cocktail = new WP_Query( array(
					'post_type' => 'cocktail',   /* edit this line */
					'posts_per_page' => 4,
					'order' => 'DESC') );
	?>

	<?php
	$counter = 5;
	while ( $cocktail->have_posts() ) : $cocktail->the_post(); ?>

	 <?php
					$bp00 = get_field('cocktail_img');
					$bp0 = get_field('cocktail_icon');
					$bp2 = get_field('cocktail_sub');
					$bp3 = get_field('cocktail_name');
					$bp4 = get_field('cocktail_use');
					$bp5 = get_field('cocktail_list');
					$bp6 = get_field('cocktail_ing_slide');
					$bp7 = get_field('cocktail_ing');
					$bpcolor = get_field('color_back');
					$bpcolor2 = get_field('color_text');
				?>
				<div class="cocktail ingS<?php echo $counter ?>" data-aos="fade-up">
					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<div class="cocktailIcon" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;">
						<img class="icon" src="<?php echo $bp0;?>">
					</div>




						<div class="ingSec ingX<?php echo $counter ?>" id="<?php echo $bpcolor;?>">
							<div class="cocktailInfo">
								<h5><?php echo $bp4;?></h5>
								<p class="useList"><?php echo $bp5;?></p>
							</div>
						</div>
						<div class="ingSlide" id="<?php echo $bpcolor;?>">
							<h2 ><?php echo $bp3;?></h2>
						</div>

				</div>
				<?php $counter++; ?>
	<?php endwhile; ?>


</section>
<a class="callButton" href="/cocktails/" data-aos="zoom-in">See All Cocktails</a>


<section class="steps" data-aos="fade-up">
	<div class="stepsFlex">
		<div class="stepsDiv" id="first">
			<a href="/cocktail/"><img src="/wp-content/themes/k8y_101/library/images/dropIcon@2x.png" alt="IV Bag Icon"></a>
			<a href="/cocktail/"><h2>Pick Your Cocktail.</h2></a>
		</div>
		<div class="stepsDiv" id="middle">
			<a href="/book/"><img src="/wp-content/themes/k8y_101/library/images/calendarIcon@2x.png" alt="IV Bag Icon"></a>
			<a href="/book/"><h2>Book an Appointment.</h2></a>
		</div>
		<div class="stepsDiv" id="last">
			<h1>Rejuvenate.</h1>
			<h1>Restore.</h1>
			<h1>Rehydrate.</h1>
		</div>
	</div>
</section>
<!--
<section class="section">
	<div class="flex">
		<div class="flexDiv"></div>
		<div class="flexDiv"></div>
	</div>
</section>
	-->

</div>

</body>


<?php get_footer(); ?>
