<?php get_header(); ?>
<section class="videoPost"  data-aos="fade-up">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="videoGrad"></div>
	<div class="videoContainer" data-aos="fade-up">
	<div class="embed-container">
		<iframe src="<?php the_field('youtube_link'); ?>" frameborder="0" allowfullscreen></iframe>
	</div>
</div>

	<div class="videoContent">
		<div class="titleInfo">
		<h1><?php the_title(); ?></h1>
		<h5><?php the_date(); ?></h5>
	</div>
		<p><?php the_content(); ?></p>
	</div>

<?php endwhile; ?>
<?php endif; ?>
</section>

<?php get_footer(); ?>
