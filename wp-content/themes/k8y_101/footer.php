			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

					<section class="footerMenu">
						<div class="footWrap">
						<ul>
								<li id="footNav"><a href="/cocktails/" target="_self">COCKTAILS</a></li>
								<li id="footNav"><a href="/pricing/" target="_self">PRICING</a></li>
								<li id="footNav"><a href="/media/" target="_self">MEDIA</a></li>
								<li id="footNav"><a href="/about/" target="_self">ABOUT</a></li>
								<li id="footNav"><a href="/faq/" target="_self">FAQ</a></li>
								<li id="footNav"><a href="/contact/" target="_self">CONTACT</a></li>
								<li id="bookNowF"><a href="/book/" target="_self">BOOK NOW</a></li>
						</ul>

						<div class="footInfoF">
							<div class="footInfoD">
								<h3>hours</h3>
								<p>Mon - Sat | 10am - 6pm</p>
								<p>Sunday | Closed</p>
							</div>x
							<div class="footInfoD">
								<h3>contact</h3>
								<p><a href="tel:12622771018">262.277.1018</a></p>
								<p><a href="mailto:h2o@hydrationoasis.com">h2o@hydrationoasis.com</a></p>
							</div>
							<div class="footInfoD">
								<h3>address</h3>
								<p>17110 W Greenfield Ave.</p>
								<p>Brookfield, WI 53005</p>
							</div>
						</div>
					</div>
					</section>

				<script src='/wp-content/themes/k8y_101/library/js/slick.min.js'></script>
				<script src='/wp-content/themes/k8y_101/library/js/scripts.js'></script>

				<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.14.2/TweenMax.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js" rel="stylesheet"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js" rel="stylesheet"></script>
				<script src="/wp-content/themes/k8y_101/library/js/drawSVG.js" rel="stylesheet"></script>
				<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
				<script src="/wp-content/themes/k8y_101/library/js/aos.js" rel="stylesheet"></script>


			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
