
<!------------------
  ------------------
      2 Columns	
  ------------------
  ------------------>

<div class="contactFlex">
	<div class="flex">
		<div class="flexDiv"><h4>Or Contact Us Directly</h4><h4>414.274.0633<br/>weddings@macwi.org</h4>
		<p>The best way to see if MAC is right for your special day is to come visit us! One of our wedding consultants will give you a tour of our venues and discuss options and pricing. We encourage you to bring your future spouse, parents, family or friends. </p>
		</div>
		<div class="flexDiv">
		<?php echo do_shortcode('[ninja_form id=1]'); ?>
		</div>
	</div>
</div>	
	
<!------------------
  ------------------
  1 Column w/ Social	
  ------------------
  ------------------>	
  							
<section class="contactSec">
	<div><?php echo do_shortcode("[ninja_form id=2]"); ?></div>
	<div class="contactInfo">
		<div>
		<a href="tel:6086283286" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i></p></a>
		<a href="tel:6086283286" target="_blank"><p>608.628.3286</p></a>
		</div>
		<div>
		<a href="mailto:richie.burke@gogeddit.com" target="_blank"><p><i class="fa fa-envelope" aria-hidden="true"></i></p></a>
		<a href="mailto:richie.burke@gogeddit.com" target="_blank"><p>richie.burke@gogeddit.com</p></a>
		</div>
	</div>
	<div class="socialCont">
		<div>
		<a href="https://www.facebook.com/richie.burke.14" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
		</div>
		<div>
		<a href="https://www.instagram.com/richie_gogeddit/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
		</div>
		<div>
		<a href="https://www.linkedin.com/in/richieburke/" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
		</div>
		<div>
		<a href="https://twitter.com/richiepburke?lang=en" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
		</div>
		<div>
		<a href="https://www.youtube.com/channel/UCpl2xpA5RN0sHQwGuS4n1gg" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
		</div>
	</div>
</section>						

<!-- BOX FORM	

	<div class="contact">
		<div class="contactForm boxContact">
			<?php echo do_shortcode('[contact-form-7 id="14" title="Contact form 1"]'); ?>
		</div>
	</div>


	
-->	