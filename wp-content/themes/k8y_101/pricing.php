<?php
/**
 Template Name: Pricing
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Pricing | H2O</title>
	<meta name="description" content="H2O pricing and membership information.">
</head>

<body>
	<div class="priceWrap" id="wrapper">
<section class="headerSec" data-aos="fade-left">
	<h2>Pricing</h2>
</section>

<section class="memberSec">


<div class="flex" data-aos="fade-up">
	<div class="flexDiv" id="orangeOut">
		<div class="priceTop"><h2>$ COCKTAIL $</h2></div>
		<div><h2 >$ MINI  $</h2></div>
	</div>

			<div class="flexDiv" id="tealOut">
				<img src="/wp-content/themes/k8y_101/library/images/nonmember@2x.png" alt="Non Member Icon"/>
				<h2 style="margin-bottom: 50px;">NON MEMBER</h2>
				<p>Not sure you want to commit to a full membership yet? Come give us a try! You can add a membership to your existing account at any time.</p>
			</div>
			<div class="flexDiv" id="greenOut">
				<img src="/wp-content/themes/k8y_101/library/images/premier@2x.png" alt="Premier Member Icon"/>
				<h2>PREMIER MEMBER</h2>
				<h3>$49 Per Month</h3>
				<p>Includes Exclusive pricing, 1 Invigorate per month + 1 Invigorate Gift per year + Unlimited Free O2 Therapy: >$1500 value in benefits </p>
<!--
				<div class="greenButton">
				< ?php echo do_shortcode('[purchase_link id="192" text="Purchase" ]'); ?>
				</div>
-->
			</div>
			<div class="flexDiv" id="purpleOut">
				<img src="/wp-content/themes/k8y_101/library/images/vip@2x.png" alt="VIP Member Icon"/>
				<h2>VIP MEMBER</h2>
				<h3>$99 Per Month</h3>
				<p>Includes MOST EXCLUSIVE pricing, 1 Invigorate Ultra per month + 1 Invigorate gift per year + Free choice of Booster per Month, +unlimited Free O2 Therapy: >$2800 value in benefits</p>

<!--

				<div class="purpleButton">
				< ?php echo do_shortcode('[purchase_link id="188" text="Purchase" ]'); ?>
				</div>
-->
			</div>
</div>
</section>
<section class="pricingSec">
	<div class="flex" data-aos="fade-up">
		<div class="flexDiv" id="memIco">
			<img src="/wp-content/themes/k8y_101/library/images/cocktailIco@2x.png" alt="Cocktail Icon"/>
		</div>
				<div class="flexDiv" id="memIco">
					<img src="/wp-content/themes/k8y_101/library/images/nonmember@2x.png" alt="Non Member Icon"/>
				</div>
				<div class="flexDiv" id="memIco">
					<img src="/wp-content/themes/k8y_101/library/images/premier@2x.png" alt="Premier Member Icon"/>
				</div>
				<div class="flexDiv" id="memIco">
					<img src="/wp-content/themes/k8y_101/library/images/vip@2x.png" alt="VIP Member Icon"/>
	</div>
</div>


		<?php
		  if( have_rows('price_chart') ):
		  while( have_rows('price_chart') ): the_row(); ?>

			<div class="flex" data-aos="fade-up">
			<div class="flexDiv">
				<p class="cocktailName"><?php the_sub_field('cocktail_name'); ?></p>
			</div>
					<div class="flexDiv">
						<div class="topP"><p><?php the_sub_field('non_member'); ?></p></div>
						<div class="bottomP"><p><?php the_sub_field('non_member2'); ?></p></div>
					</div>
					<div class="flexDiv">
						<div class="topP"><p><?php the_sub_field('premier'); ?></p></div>
						<div class="bottomP"><p><?php the_sub_field('premier2'); ?></p></div>
					</div>
					<div class="flexDiv">
						<div class="topP"><p><?php the_sub_field('vip'); ?></p></div>
						<div class="bottomP"><p><?php the_sub_field('vip2'); ?></p></div>
					</div>
</div>
			<?php endwhile; endif; ?>
			<p class="asterisk"><?php the_field('ast1'); ?></p>
</section>

<section class="headerSec" data-aos="fade-left">
	<h2>Other Services</h2>
</section>

<section class="pricingSec">
	<div class="flex" data-aos="fade-up">
		<div class="flexDiv" id="memIco">
			<img src="/wp-content/themes/k8y_101/library/images/otherServ@2x.png" alt="Other Services Icon"/>
		</div>
				<div class="flexDiv" id="memIco">
					<img src="/wp-content/themes/k8y_101/library/images/nonmember@2x.png" alt="Non Member Icon"/>
				</div>
				<div class="flexDiv" id="memIco">
					<img src="/wp-content/themes/k8y_101/library/images/premier@2x.png" alt="Premier Member Icon"/>
				</div>
				<div class="flexDiv" id="memIco">
					<img src="/wp-content/themes/k8y_101/library/images/vip@2x.png" alt="VIP Member Icon"/>
	</div>
</div>

		<?php
		  if( have_rows('other_serv') ):
		  while( have_rows('other_serv') ): the_row(); ?>

			<div class="flex" data-aos="fade-up">
			<div class="flexDiv">
				<p class="cocktailName"><?php the_sub_field('service_name'); ?></p>
			</div>
					<div class="flexDiv">
						<p><?php the_sub_field('non_memberS'); ?></p>
					</div>
					<div class="flexDiv">
						<p><?php the_sub_field('premierS'); ?></p>
					</div>
					<div class="flexDiv">
						<p><?php the_sub_field('vipS'); ?></p>
					</div>
			</div>
			<?php endwhile; endif; ?>
			<p class="asterisk"><?php the_field('ast2'); ?></p>

</section>


</div>
</body>



<?php get_footer(); ?>
