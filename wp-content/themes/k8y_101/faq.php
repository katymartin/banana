<?php
/**
 Template Name: FAQ
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>About | H2O</title>
	<meta name="description" content="Learn about our staff and how we became one of the first hyrdration therapy locations in Milwaukee.">
</head>

<body>
	<div id="wrapper">
<section class="headerSec" data-aos="fade-left">
	<h2>FAQ</h2>
</section>

<section class="faq" data-aos="fade-up">
	<div class="faqFlex">
	<?php
		if( have_rows('faq') ):
		while( have_rows('faq') ): the_row(); ?>

			<div class="faqQA">
				<h2><span id="faqQ">Q:</span><?php the_sub_field('question') ;?></h2>
				<p><span id="faqA">A:</span><?php the_sub_field('answer') ;?></p>
			</div>

		<?php endwhile; endif; ?>
		</div>
<div id="break"></div>

		<div class="faqFull" data-aos="fade-up">
		<?php
			if( have_rows('faq_full') ):
			while( have_rows('faq_full') ): the_row(); ?>

				<div class="faqQAfull">
					<h2><?php the_sub_field('question_full') ;?></h2>
					<p><?php the_sub_field('answer_full') ;?></p>
				</div>

			<?php endwhile; endif; ?>
			</div>
</section>

</div>
</body>



<?php get_footer(); ?>
