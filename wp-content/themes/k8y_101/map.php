<!DOCTYPE html>
<html>
    <head>
    <body>

       <div class="mapHeight">
       <div id="map"></div>
       <a href="https://www.google.com/maps/place/17110+W+Greenfield+Ave+%236,+Brookfield,+WI+53005/@43.0169422,-88.1282744,17z/data=!3m1!4b1!4m5!3m4!1s0x88050633b079296d:0x676bde1817613055!8m2!3d43.0169422!4d-88.1260804" target="_blank" class="mapButton">View In Maps</a>






        <script type="text/javascript">
            // When the window has finished loading create our google map below

			google.maps.event.addDomListener(window, 'load', init);
        	function initMap() {

        	var uluru = {lat: 43.0169422, lng: -88.1282744};
        	var map = new google.maps.Map(document.getElementById('map'), {
          	zoom: 4,
          	center: uluru
        	});


				var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 12,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(43.0169422, -88.1282744), // New York


                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                     styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#f68842"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#ece9e6"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#f68842"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#ece9e6"},{"lightness":21}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#7e695c"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#ece9e6"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]}]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>

                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(43.0169422, -88.1282744),
                    map: map,
                    title: 'Snazzy!'
                });
            }
        </script>
        </div></div>
        <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByQezdv2K8KtJh9Rdh1NSnfVdsWXX_rsg&callback=initMap">
    </script>
     </body>
    </head>






</html>
